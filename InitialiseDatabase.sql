﻿USE [F:\BANKSYSAN\DEVELOPMENT\REACH.NET\SOURCE\BANKSYSAN.DEMO.REACHDOTNET\TODOAPPLICATION.MDF]
GO

/****** Object: Table [dbo].[ToDoItems] Script Date: 05/07/2015 18:07:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


GO
CREATE TABLE [dbo].[ToDoItems] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [IsCompleted] BIT          NOT NULL
);

GO

CREATE PROCEDURE [dbo].[ins_NewToDoItem] 
	@Description  varchar(50)

AS
	INSERT INTO ToDoItems
	VALUES (@Description, 0)


RETURN 0

GO

CREATE PROCEDURE [dbo].[sel_GetItems]
	
AS
	SELECT Id, [Description], IsCompleted
	FROM ToDoItems

RETURN 0

GO

CREATE PROCEDURE [dbo].[upd_ToDoItem]
	@Id int,
	@Description varchar(50),
	@IsCompleted bit
AS
	UPDATE ToDoItems
	SET [Description] = @Description, IsCompleted = 1
	WHERE Id = @Id

RETURN 0

GO

EXEC [dbo].[ins_NewToDoItem] 'one';
EXEC [dbo].[ins_NewToDoItem] 'two';
EXEC [dbo].[ins_NewToDoItem] 'three';
EXEC [dbo].[ins_NewToDoItem] 'four';
EXEC [dbo].[ins_NewToDoItem] 'five';