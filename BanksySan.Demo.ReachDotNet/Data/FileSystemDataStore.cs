﻿namespace BanksySan.Demo.ReachDotNet.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using ToDo;

    internal class FileSystemDataStore : IDataStore
    {
        private readonly FileInfo _dataFile;
        private readonly DataItemSerializer _serializer;

        public FileSystemDataStore(FileInfo dataFile, DataItemSerializer serializer)
        {
            if (dataFile == null)
            {
                throw new ArgumentNullException(nameof(dataFile));
            }

            _dataFile = dataFile;
            _serializer = serializer;
        }

        public void Save(ToDoItem toDoItem)
        {
            var items = ReadItems().ToList();
            var successful = items.Remove(toDoItem);
            if (successful)
            {
                items.Add(toDoItem);    
                WriteFile(items);
            }
            else
            {
                throw new InvalidOperationException($"Item with ID: {toDoItem.Id} is unknown");
            }
        }

        public void Save(ToDoInfo toDoItem)
        {
            var items = ReadItems().ToList();
            var maxId = 0;

            if (items.Any())
            {
                maxId = Items.Max(x => x.Id);
            }

            var set = new HashSet<ToDoItem>(items);

            set.Add(new ToDoItem(maxId + 1, toDoItem.Description, false));

            WriteFile(set);
        }

        public ToDoItem this[int id] => Items.SingleOrDefault(x => x.Id == id);

        public IEnumerable<ToDoItem> Items => ReadItems();

        private IEnumerable<ToDoItem> ReadItems( )
        {
            var items = new HashSet<ToDoItem>();

            using (var fileStream = _dataFile.Open(FileMode.OpenOrCreate))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    while (! streamReader.EndOfStream)
                    {
                        var line = streamReader.ReadLine();
                        var item = _serializer.Deserialize(line);
                        items.Add(item);
                    }
                }
            }

            return items;
        }

        private void WriteFile(IEnumerable<ToDoItem> items)
        {
            var stringBuilder = new StringBuilder();

            foreach (var item in items)
            {
                var serializedItem = _serializer.Serialize(item);

                stringBuilder.AppendLine(serializedItem);
            }

            _dataFile.Refresh();
            if (_dataFile.Exists)
            {
                _dataFile.Delete();
            }

            using (var writer = _dataFile.OpenWrite())
            {
                using (var streamWriter = new StreamWriter(writer))
                {
                    streamWriter.Write(stringBuilder);
                }
            }
        }
    }
}