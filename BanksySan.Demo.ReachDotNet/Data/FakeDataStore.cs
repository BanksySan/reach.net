﻿namespace BanksySan.Demo.ReachDotNet.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using ToDo;

    /// <summary>
    ///     Non persisting datastore, used for testing.
    /// </summary>
    internal class FakeDataStore : IDataStore
    {
        private readonly ISet<ToDoItem> _toDoItems = new HashSet<ToDoItem>();

        public void Save(ToDoItem toDoItem)
        {
            _toDoItems.Add(toDoItem);
        }

        public void Save(ToDoInfo toDoInfo)
        {
            var newId = _toDoItems.Any() ? _toDoItems.Max(x => x.Id) + 1 : 1;
            var toDoItem = new ToDoItem(newId, toDoInfo.Description, toDoInfo.Completed);
            _toDoItems.Add(toDoItem);
        }

        public ToDoItem this[int id]
        {
            get { return _toDoItems.SingleOrDefault(x => x.Id == id); }
        }

        public IEnumerable<ToDoItem> Items => _toDoItems;
    }
}