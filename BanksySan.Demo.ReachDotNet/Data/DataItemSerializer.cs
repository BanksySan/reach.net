﻿namespace BanksySan.Demo.ReachDotNet.Data
{
    using ToDo;

    internal class DataItemSerializer
    {
        public string Serialize(ToDoItem toDoItem)
        {
            return $"{toDoItem.Id}|{toDoItem.Description}|{toDoItem.Completed}";
        }

        public ToDoItem Deserialize(string data)
        {
            var split = data.Split('|');
            var id = int.Parse(split[0]);
            var description = split[1];
            var completed = bool.Parse(split[2]);

            return new ToDoItem(id, description, completed);
        }
    }
}