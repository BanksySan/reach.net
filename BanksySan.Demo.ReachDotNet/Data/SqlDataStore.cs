﻿namespace BanksySan.Demo.ReachDotNet
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using Data;
    using ToDo;

    internal class SqlDataStore : IDataStore
    {
        private readonly string _connectionString;

        public SqlDataStore(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Save(ToDoItem toDoItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "upd_ToDoItem";

                var idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.SqlDbType = SqlDbType.Int;
                idParameter.Value = toDoItem.Id;

                var descriptionParameter = command.CreateParameter();
                descriptionParameter.ParameterName = "@Description";
                descriptionParameter.SqlDbType = SqlDbType.VarChar;
                descriptionParameter.Size = 50;
                descriptionParameter.Value = toDoItem.Description;

                var completedParameter = command.CreateParameter();
                completedParameter.ParameterName = "@IsCompleted";
                completedParameter.SqlDbType = SqlDbType.Bit;
                completedParameter.Value = toDoItem.Completed;

                command.Parameters.AddRange(new[] { idParameter, descriptionParameter, completedParameter });

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public void Save(ToDoInfo toDoItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "ins_NewToDoItem";

                var descriptionParameter = command.CreateParameter();
                descriptionParameter.ParameterName = "@Description";
                descriptionParameter.SqlDbType = SqlDbType.VarChar;
                descriptionParameter.Size = 50;
                descriptionParameter.Value = toDoItem.Description;

                command.Parameters.Add(descriptionParameter);
                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public ToDoItem this[int id] => Items.SingleOrDefault(x => x.Id == id);

        public IEnumerable<ToDoItem> Items
        {
            get
            {
                var dataTable = new DataTable();

                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "[sel_GetItems]";

                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            dataTable.Load(reader);
                        }
                    }
                }

                var items = dataTable.AsEnumerable().Select(x =>
                {
                    var id = x.Field<int>("Id");
                    var description = x.Field<string>("Description");
                    var isCompleted = x.Field<bool>("IsCompleted");

                    return new ToDoItem(id, description, isCompleted);
                });

                return items;
            }
        }
    }
}