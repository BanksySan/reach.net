﻿namespace BanksySan.Demo.ReachDotNet.Data
{
    using System.Collections.Generic;
    using ToDo;

    internal interface IDataStore
    {
        void Save(ToDoItem toDoItem);
        void Save(ToDoInfo toDoItem);

        ToDoItem this[int id] { get; }

        IEnumerable<ToDoItem> Items { get; }
    }
}