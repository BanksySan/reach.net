﻿namespace BanksySan.Demo.ReachDotNet.ToDo
{
    internal class ToDoInfo
    {
        public ToDoInfo(string description)
        {
            Description = description;
            Completed = false;
        }

        public string Description { get; private set; }

        public bool Completed { get; protected set; }
    }
}