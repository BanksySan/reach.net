﻿namespace BanksySan.Demo.ReachDotNet.ToDo
{
    public enum UserActions
    {
        Create,
        ListAll,
        Exit,
        MarkComplete
    }
}