﻿namespace BanksySan.Demo.ReachDotNet.ToDo
{
    using System;

    internal class ToDoItem : ToDoInfo, IEquatable<ToDoItem>
    {
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(ToDoItem other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return Id == other.Id;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <returns>
        /// true if the specified object  is equal to the current object; otherwise, false.
        /// </returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var other = obj as ToDoItem;
            return other != null && Equals(other);
        }

        /// <summary>
        /// Serves as the default hash function. 
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode( )
        {
            return Id;
        }

        public static bool operator ==(ToDoItem left, ToDoItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ToDoItem left, ToDoItem right)
        {
            return ! Equals(left, right);
        }

        public ToDoItem(int id, string description, bool completed)
            : base(description)
        {
            Id = id;
            Completed = completed;
        }

        public int Id { get; private set; }

        public void MarkCompleted( )
        {
            Completed = true;
        }
    }
}