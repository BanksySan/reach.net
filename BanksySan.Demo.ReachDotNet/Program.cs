﻿namespace BanksySan.Demo.ReachDotNet
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using ToDo;

    internal class Program
    {
        private const int DEFAULT_WIDTH = 50;
        private const string APPLICATION_TITLE = "To Do Application";
        private static readonly IDictionary<string, UserActions> USER_ACTIONS;
        private static readonly IDataStore DATA_STORE;

        static Program( )
        {
            USER_ACTIONS = new Dictionary<string, UserActions>
            {
                { "1", UserActions.Create },
                { "2", UserActions.ListAll },
                { "3", UserActions.MarkComplete },
                { "4", UserActions.Exit }
            };

            const string DB_FILE_PATH =
                @"<<ADD PATH TO MDF FILE HERE>>";

            const string CONNECTION_STRING =
                @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + DB_FILE_PATH +
                ";Integrated Security=True;Connect Timeout=30";

            DATA_STORE = new SqlDataStore(CONNECTION_STRING);

            Console.WindowWidth = 50;
            Console.BufferWidth = 100;
            Console.SetWindowSize(Console.BufferWidth, Console.WindowHeight);
        }

        private static void Main(string[] args)
        {
            Console.Title = APPLICATION_TITLE;

            Console.WindowWidth = DEFAULT_WIDTH;
            Console.BufferWidth = DEFAULT_WIDTH;
            Console.WriteLine("Welcome to your {0}", APPLICATION_TITLE);
            Console.WriteLine();
            UserActions userActions;

            while ((userActions = GetUserAction()) != UserActions.Exit)
            {
                switch (userActions)
                {
                    case UserActions.Create :
                        CreateNewToDoItem();
                        break;
                    case UserActions.ListAll :
                        ListAllItems();
                        break;
                    case UserActions.MarkComplete :
                        MarkItemComplete();
                        break;
                    default :
                        throw new ArgumentOutOfRangeException(nameof(userActions));
                }
            }
        }

        private static void MarkItemComplete( )
        {
            Console.Clear();
            Console.WriteLine("Enter the number of the item to mark complete.");
            ListAllItems();
            var userInput = Console.ReadLine();
            var itemNumber = int.Parse(userInput);
            var item = DATA_STORE[itemNumber];
            item.MarkCompleted();
            DATA_STORE.Save(item);
        }

        private static void ListAllItems( )
        {
            Console.Clear();
            Console.WriteLine("List of all To Do Items");
            Console.WriteLine();

            var items = DATA_STORE.Items;
            var orderedItems = items.OrderBy(x => x.Id);
            var originalConsoleColour = Console.ForegroundColor;

            foreach (var toDoItem in orderedItems)
            {
                var itemAsString = $"{toDoItem.Id}: {toDoItem.Description}".PadRight(20) + "Completed: ";

                Console.Write(itemAsString);
                if (toDoItem.Completed)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.WriteLine(toDoItem.Completed);
                Console.ForegroundColor = originalConsoleColour;
            }
        }

        private static void CreateNewToDoItem( )
        {
            Console.Clear();
            Console.WriteLine("Create new To Do item");
            Console.WriteLine("Enter a description");
            var description = Console.ReadLine();
            var newItem = new ToDoInfo(description);

            DATA_STORE.Save(newItem);
        }

        private static UserActions GetUserAction( )
        {
            Console.WriteLine();
            Console.WriteLine("What would you like to do?");
            Console.WriteLine("1. Enter a new item.");
            Console.WriteLine("2. View all To Do items.");
            Console.WriteLine("3. MarkComplete");
            Console.WriteLine("4. Exit");

            var userSelection = Console.ReadLine();

            try
            {
                return USER_ACTIONS[userSelection];
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("That isn't a valid option.  Please try again.");
                return GetUserAction();
            }
        }
    }
}